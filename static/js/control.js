////////
//
//      Initilizing the antenna sockets
//
////////

var namespace = '/telescope'; // change to an empty string to use the global namespace
// the socket.io documentation recommends sending an explicit package upon connection
// this is specially important when using the global namespace
var socket = io.connect('http://' + document.domain + ':' + location.port + namespace);
console.log('http://' + document.domain + ':' + location.port + namespace);
socket.on('connect', function(){
    socket.emit('my event', {data: 'I\'m connected!'});
    console.log('conectado');
});

////////
//
//      Changing Observing modes
//
////////
/*
$(document).ready(function(){
	$('#link').click(function(event){
			if(confirm('Observing mode to SD SRT1. Are you sure?')){
				$.ajax("/link").done(function (reply) {
					$('#stuff').html(reply);
				});}
			});

	$('#singleimg1').click(function(event){
			if(confirm('Observing mode to SD SRT1. Are you sure?')){
				$.ajax("/SD_SRT1").done(function (reply) {
					$('#maincontainer').html(reply);
				});}
			});
});
*/
/*$(document).ready(function(){
	$('#singleimg1').click(function(event){
		if(confirm('You are about to change the obsmode, and enter to SD SRT1 Mode. Are you sure?')){
		window.location.href = 'telescope';
		socket.emit('changeObsModeEvent',{1:'SD',2:'SRT1'});
		}
	});

	$('#singleimg2').click(function(event){
		if(confirm('You are about to change the obsmode, and enter to SD SRT2 Mode. Are you sure?')){
		socket.emit('changeObsModeEvent',{1:'SD',2:'SRT2'});
		return false;
		}
	});

	$('#adder').click(function(event){
		socket.emit('changeObsModeEvent',{1:'SD',2:'SRT1'});
		return false;
	});
});*/
/*
$('#singleimg').click(function(event){
	socket.emit('changeObsModeEvent',{1:'SD',2:'SRT1'});
	return false;
});*/
/*
var popup;
var plotDaemon;
var freqDaemon;
var coordDaemon;
var refreshTime = 1000;
window.setInterval(function(){refreshHTMLClocks();},refreshTime);
*/
////////
//
//       handling incoming requests
//
////////
/*socket.on('changeObsModeReadyEvent', function(mode){
	clearInterval(plotDaemon);
	clearInterval(freqDaemon);
	clearInterval(coordDaemon);
	plotDaemon = window.setInterval(function(){refreshHTMLPlots();},refreshTime);
	freqDaemon = window.setInterval(function(){refreshHTMLFreqs();},refreshTime);
	coordDaemon = window.setInterval(function(){refreshHTMLCoords();},refreshTime);
});*/
var currentMode;

socket.on('receiveSpectraEvent', function(msg) {
	//$.plot("#instaspectra", [[[0, 12], [7, 12], null, [7, 2.5], [12, 2.5]]]);
	console.log('receiveSpectraEvent');
	console.log(msg)
	message = $.parseJSON(msg);
	console.log(message);
	console.log("from contro.js, console.log(message.SRT);")
	console.log(message.SRT);
	data_srt1.parseSpectra(message.SRT);
	console.log(data_srt1);
	$.plot("#spectra", 
		[
			{label: "spec",	data : data_srt1.parsedSpectrum},
			{label: "intspec",	data: data_srt1.parsedIntspectrum}
		], 
		{
			yaxis: {
				axisLabel: 'Power Spectral Density (k·V^2·Hz^-1)',
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
				axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
				axisLabelPadding: 5,
				tickDecimals: 0,
				min: 0,
				transform: function(x){ return x/1000000;},
				tickFormatter: function(x){ return x/1000000;}
			},
			xaxis: {
				axisLabel: 'Frequency (MHz)',
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
    			axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
    			axisLabelPadding: 5,
    			/*min: data_srt1.xfreq[0] - 0.5*data_srt1.sampleStamp.freqsep*10e-3,
    			max: data_srt1.xfreq[255] + 0.5*data_srt1.sampleStamp.freqsep*10e-3,
			*/},/*,
			bars:{
				show: true,
				barWidth: freq.span*2/finalData[0].length,
				//align: center,
			}*/
		});
	//console.log(freq.center - 0.5*freq.span, freq.center + 0.5*freq.span);
});

socket.on('receiveArrayStatusEvent', function(msg) {
	st = $.parseJSON(msg);
	st = st.SRT1;
	//console.log(st);
	console.log('EVENT: receiveArrayStatusEvent');
	refreshHTMLCoords(st.az, st.el, st.aznow, st.elnow, st.ra, st.dec, st.ranow, st.decnow, st.azoffset, st.eloffset);
	refreshHTMLGeneralInfo(st.slewing, st.tracking, st.SRTTarget, st.SRTTrack);
});

socket.on('receiveApiStatusEvent', function(msg) {
	apiStatus = $.parseJSON(msg);
	//console.log(apiStatus);
});

socket.on('receiveObsModeStatusEvent', function(msg) {
	obsModeStatus = $.parseJSON(msg);
	console.log(obsModeStatus);
	currentMode = obsModeStatus.observingMode;
	console.log(currentMode);
	//console.log(obsModeStatus);
});

socket.on('testResponse', function(msg){
	console.log(msg.data);
});

////////
//
//       HTML Refreshers
//
////////
function refreshHTMLCoords(az,el,aznow,elnow,ra,dec,ranow,decnow,azoff,eloff){
	document.getElementById('HTMLaz').innerHTML = az.toFixed(2);
	document.getElementById('HTMLel').innerHTML = el.toFixed(2);
	document.getElementById('HTMLaznow').innerHTML = aznow.toFixed(2);
	document.getElementById('HTMLelnow').innerHTML = elnow.toFixed(2);
	document.getElementById('HTMLra').innerHTML = ra.toFixed(2);
	document.getElementById('HTMLdec').innerHTML = dec.toFixed(2);
	document.getElementById('HTMLranow').innerHTML = ranow.toFixed(2);
	document.getElementById('HTMLdecnow').innerHTML = decnow.toFixed(2);
	document.getElementById('HTMLazoffset').innerHTML = azoff.toFixed(2);
	document.getElementById('HTMLeloffset').innerHTML = eloff.toFixed(2);
}


function refreshHTMLFreqs(freq0,freqsep,temp){
	document.getElementById('HTMLfreq0').innerHTML = freq0;
	document.getElementById('HTMLfreqsep').innerHTML = freqsep;
	document.getElementById('HTMLtemperature').innerHTML = temp;
}

function refreshHTMLGeneralInfo(slewing,tracking,SRTTarget,SRTTrack){
	document.getElementById('HTMLslewing').innerHTML = semaphore('HTMLslewing',slewing);
	document.getElementById('HTMLtracking').innerHTML = semaphore('HTMLtracking',tracking);
	document.getElementById('HTMLtargetmode').innerHTML = semaphore('HTMLtargetmode',SRTTarget);
	document.getElementById('HTMLtarget').innerHTML = SRTTrack;
}

function refreshHTMLClocks(){
	document.getElementById('HTMLlst').innerHTML = currentClockObj.toLocaleTimeString();
	document.getElementById('HTMLutc').innerHTML = currentClockObj.getUTCHours() + ":"+ currentClockObj.getUTCMinutes()+ ":" + currentClockObj.getUTCSeconds();
}
/*
function semaphore(id, cond){
	if(cond){
		return 'true';
	}
	else{
		return 'false';
	}
}*/

function semaphore(id, cond){ // mas bonito asi :)
	if(cond){
		return '<img src=\'../static/res/icons/green.png\' id=\'semaphore\'>';
	}
	else{
		return '<img src=\'../static/res/icons/red.png\' id=\'semaphore\'>';
	}
}
/*
function semaphore(id, cond){ // mas bonito asi :)
	if(cond){
		return '<img src=\'{{ url_for(\'static\', filename=\'res/icons/green.png\') }}\'>';
	}
	else{
		return '<img src=\'{{ url_for(\'static\', filename=\'res/icons/red.png\') }}\'>';
	}
}*/

////////
//
//       Popups
//
////////
function requestCoords(){
	popup = window.open("","MsgWindow","width=300,height=400");
	popup.document.write('<p>Enter coordinates:</p>');
	popup.document.write('<form name="coords">');
	popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichcoord" id="azel">AZ EL</legend>');
	popup.document.write('<span>AZ</span><input type="number" name="az" value="0"><br>');
	popup.document.write('<span>EL</span><input type="number" name="el" value="0">');
	popup.document.write('</fieldset>');
	/*popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichcoord" id="radec">RA DEC</legend>');
	popup.document.write('<span>RA</span><input type="number" name="ra" value="0"><br>');
	popup.document.write('<span>DEC</span><input type="number" name="dec" value="0">');
	popup.document.write('</fieldset>');*/
	popup.document.write('<fieldset>'); /// TODO TODO TODO!! IMPLEMENT RA DEC CONVERSION
	popup.document.write('<legend><input type="radio" name="whichcoord" id="tar">Or choose an -<strong><em>now visible</em></strong>- target:</legend>');
	popup.document.write('<select name="targetList"></select>');
	popup.document.write('</fieldset>');
	popup.document.write('<input type="button" name="sub" Value="OK" onClick="if(document.getElementById(\'azel\').checked){window.opener.socket.emit(\'makeObsEvent\',{mode: \'GoTo\', target: [document.coords.az.value,document.coords.el.value]});}else if(document.getElementById(\'radec\').checked){window.opener.socket.emit(\'makeObsEvent\',{mode: \'Track\', target: [parseFloat(document.coords.ra.value),parseFloat(document.coords.dec.value)]});}else if(document.getElementById(\'tar\').checked){window.opener.socket.emit(\'makeObsEvent\',{mode: \'Track\', target: String(document.coords.targetList.value)});}window.close();">');
	popup.document.write('<input type="button" name="cancel" Value="cancel" onClick="window.close();">');
	popup.document.write('</form>');
	targets = apiStatus.radecSources;
	console.log(popup);
	
	//	console.log(popup.document.coordinates.elements())
	for (var i = targets.length - 1; i >= 0; i--) {
		//l=popup.document.coords.targetList.options.length;
		var option = popup.document.createElement("option");
		source = targets[i];
		name = source.source;
		el = parseInt(source.el);
		az = parseInt(source.az);
		option.text = name + ', az:'+ az.toString() + ', el:' + el.toString();
		option.value = name;
		popup.document.coords.targetList.add(option);
	}
}
/*
function requestFreq(){
	popup = window.open("","MsgWindow","width=300,height=400");
	popup.document.write('<p>Enter frequency:</p>');
	popup.document.write('<form name="freqs" id="currentForm">');
	popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichfreq" id="censpan">Center Freq & Span</legend>');
	popup.document.write('<span>Center (MHz):</span><input type="number" name="center"><br>');
	popup.document.write('<span>Span (MHz):</span><input type="number" name="span" value="">');
	popup.document.write('</fieldset>');
	popup.document.write('<fieldset>');
	popup.document.write('<legend><input type="radio" name="whichfreq" id="startstop">Start & Stop Frequencies</legend>');
	popup.document.write('<span>Start (MHz):</span><input type="number" name="start" value="0"><br>');
	popup.document.write('<span>Stop (MHz):</span><input type="number" name="stop" value="0">');
	popup.document.write('</fieldset>');
	popup.document.write('<input type="button" name="sub" Value="OK" onClick="if(document.getElementById(\'censpan\').checked){window.opener.execCommand(\'freq censpan \'+String(document.freqs.center.value)+\' \'+String(document.freqs.span.value));}else if(document.getElementById(\'startstop\').checked){window.opener.execCommand(\'freq startstop \'+String(document.freqs.start.value)+\' \'+String(document.freqs.stop.value));}window.close();">');
	popup.document.write('<input type="button" name="cancel" Value="cancel" onClick="window.close();">');
	popup.document.write('</form>');
	//document.getElementById("currentForm").elements["center"].value=window.opener.freq.center;
}*/


function popupCommand(command){
	console.log("popupCommand");
	switch(command){
		case 'coords':
			requestCoords();
			break;
		case 'offset':
			requestOffset();
			break;
		case 'freq':
			requestFreq();
			break;
		}
}

function openUniquePopup(command){
	console.log(command);
	//console.log(popup.closed, typeof(popup));
	if(typeof(popup)=='undefined' || popup.closed){
		popupCommand(command);
	}
	else{
		try{console.log("try");popup.document;}
		catch(e){popupCommand(command);}

		if (navigator.appName == 'Microsoft Internet Explorer') {
				popup.close();
				popupCommand(command);
			}
		else{popup.focus();}
	}
}

function requestOffset(){
	popup = window.open("","MsgWindow","width=300,height=200");
	popup.document.write('<p>Enter AZEL offsets:</p>');
	popup.document.write('<form name="azel">');
	popup.document.write('<fieldset>');
	popup.document.write('<span>AZ</span><input type="number" name="az" value="0"><br>');
	popup.document.write('<span>EL</span><input type="number" name="el" value="0">');
	popup.document.write('<input type="button" name="sub" Value="OK" onClick="window.opener.socket.emit(\'offsetEvent\', {az: this.azel.az.value, el: this.azel.el.value});window.close();">');
	popup.document.write('</fieldset>');
	popup.document.write('</form>');
}

function requestFreq(){
	popup = window.open("","MsgWindow","width=300,height=400");
	popup.document.write('<style media="screen" type="text/css">fieldset {font-family: monospace;}</style>');
	popup.document.write('<p>Select frequency mode:</p>');
	popup.document.write('<form name="freqs" id="currentForm">');
	popup.document.write('<fieldset>');
	popup.document.write('span>Frequency (MHz):</span><input type="number" name="freq"><br>');
	popup.document.write('<input type="radio" name="whichfreq" id="op0">0: nfreq:500, freqsep:0.04, intg:0.1<br>');
	popup.document.write('<input type="radio" name="whichfreq" id="op1">1: nfreq:64, freqsep:0.0078125, intg:0.1<br>');
	popup.document.write('<input type="radio" name="whichfreq" id="op2">2: nfreq:64, freqsep:0.00390625, intg:1.04976<br>');
	popup.document.write('<input type="radio" name="whichfreq" id="op3">3: nfreq:64, freqsep:0.001953125, intg:2.09952<br>');
	popup.document.write('<input type="radio" name="whichfreq" id="op4">4: nfreq:156, freqsep:0.0078125, intg:0.52488<br>');
	popup.document.write('<input type="radio" name="whichfreq" id="op5">5: nfreq:40, freqsep:1.0, intg:0.52488<br>');
	popup.document.write('<p>nfreq = The number of channels (samples) in the spectrum</p>');
	popup.document.write('<p>freqsep = The frequency separation in [KHz] between the channels in the spectrum</p>');
	popup.document.write('<p>intg = Integration time [s]</p>');
	popup.document.write('</fieldset>');
	popup.document.write('<input type="button" name="sub" Value="OK" onClick="if(document.getElementById(\'op0\').checked){window.opener.socket.emit(\'changeRxModeEvent\', {freq: document.freqs.freq.value, mode: 0});}else if(document.getElementById(\'op1\').checked){window.opener.socket.emit(\'changeRxModeEvent\', {freq: document.freqs.freq.value, mode: 1});}else if(document.getElementById(\'op2\').checked){window.opener.socket.emit(\'changeRxModeEvent\', {freq: document.freqs.freq.value, mode: 2});}else if(document.getElementById(\'op3\').checked){window.opener.socket.emit(\'changeRxModeEvent\', {freq: document.freqs.freq.value, mode: 3});}else if(document.getElementById(\'op4\').checked){window.opener.socket.emit(\'changeRxModeEvent\', {freq: document.freqs.freq.value, mode: 4});}else if(document.getElementById(\'op5\').checked){window.opener.socket.emit(\'changeRxModeEvent\', {freq: document.freq.value, mode: 5});} window.close();">');
	popup.document.write('<input type="button" name="cancel" Value="cancel" onClick="window.close();">');
	popup.document.write('</form>');
}



// var plotoptions = {
// 	color 
// }
/*


function changeMode(mode){
	var isChanging = false;
	document.getElementById('welcome').style.display = "none";
	document.getElementById('stuff').style.display = "initial";
	switch(mode){
		case "single":
			document.getElementById('stuff').innerHTML = '<h1>Single Dish Mode</h1>'+
			'<div id="spectra" class="plot"></div>';
			//initForNewMode('SD','SRT1');
			clearInterval(plotDaemon);
			clearInterval(freqDaemon);
			clearInterval(coordDaemon);
			plotDaemon = window.setInterval(function(){refreshHTMLPlots();},refreshTime);
			freqDaemon = window.setInterval(function(){refreshHTMLFreqs();},refreshTime);
			coordDaemon = window.setInterval(function(){refreshHTMLCoords();},refreshTime);
			$('#singleimg').attr('src','res/icons/single_active.gif');
			$('#corrimg').attr('src','res/icons/corr.gif');
			$('#sumimg').attr('src','res/icons/sum.gif');
			break;
		case "intensity":
			document.getElementById('stuff').innerHTML = '<h1>Intensity Mode</h1>'+
			'<div id="spectra" class="plot"></div>';
			//initForNewMode('ARI','SH');
			clearInterval(plotDaemon);
			plotDaemon = window.setInterval(function(){placePlots();},1000);
			break;
		case "corr":
			document.getElementById('stuff').innerHTML = '<h1>Correlator Mode</h1>'+
			'<div id="spectra" class="plot"></div>';
			//initForNewMode('ARI','ROACH');
			clearInterval(plotDaemon);
			plotDaemon = window.setInterval(function(){placePlots();},1000);
			break;
	}
}



function execCommand(string) {
	console.log("1");
	console.log(string);
	command = string.split(" ");
	c = command[0];
	if(c=='azel'){
		// execute command in ICE
		position.goToAzel(parseFloat(command[1]),parseFloat(command[2]));}
	else if(c=='radec'){
		position.goToRadec(parseFloat(command[1]),parseFloat(command[2]));
	}
	else if(c=='offset'){
		position.setOffset(parseFloat(command[1]),parseFloat(command[2]));
	}
	//else if(c=='target'){
		//API.SetTarget(command[1],1420.1,2);
		//API.StartTracking();}
	else if(c=='stow'){
		console.log("hola");
		position.goToAzel(0,0);
	}
	else if(c=='freq'){
		freq.changeFreq(command[1],parseFloat(command[2]),parseFloat(command[3]));
	}
}
*/
