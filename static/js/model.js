////////
//
//      Data handling functions and objects
//
////////
function dataObj(){  // all spectra will have a length of 256
	this.n = 0;// number of iterations 
	this.spectrum = []; // array for spectrum
	this.intspectrum = []; // array for integrated spectrum
	this.xfreq = [];
	this.sampleStamp = [];
	this.parsedSpectrum = [];
	this.parsedIntspectrum = [];
}
dataObj.prototype = {
	constructor: dataObj,
	parseSpectra:function(data) { // the data in JSON, only samplestamp and spectrum
		console.log('data');
		console.log(data);
		this.n++;
		console.log('this.n');
		console.log(this.n);
		this.sampleStamp = data.sampleStamp;
		this.spectrum = data.specd;
		// ahora creamos el eje X, solo una vez al obtener el primer espectro. Esto asume
		// que cada vez que cambiemos de modo o de frecuencia, el objeto debe ser reiniciado
		if(this.n==1){
			this.intspectrum = data.specd;
			for(var i=0; i <= 255; i++){
				this.xfreq.push(this.sampleStamp.freq0+i*this.sampleStamp.freqsep*10e-3); // MHZ!
			}
			console.log("this.xfreq");
			console.log(this.xfreq);
			i=1;
			console.log("this.sampleStamp.freq0+i*this.sampleStamp.freqsep*10e-3");
			console.log(this.sampleStamp.freq0+i*this.sampleStamp.freqsep*10e-3);
		}
		this.parsedSpectrum = [];
		this.parsedIntspectrum = [];
		for(var i=0; i <= 255; i++){
			this.parsedSpectrum.push([this.xfreq[i],this.spectrum[i]]);
			this.parsedIntspectrum.push([this.xfreq[i],this.intspectrum[i]]);
		}

		// sacar el 
		/*console.log("this.sampleStamp");		
		console.log(this.sampleStamp);
		console.log("this.spectrum");
		console.log(this.spectrum);
		console.log("this.intspectrum");
                console.log(this.intspectrum);*/
		var temp = multiplier(this.intspectrum,this.n);
		temp = adder(temp,this.spectrum);
		this.intspectrum = multiplier(temp,1/(this.n+1));
	},
	reset:function() {
		this.n = 0;
		this.spectrum = [];
		this.intspectrum = [];
		this.xfreq = [];
		this.sampleStamp = [];
	},
	exportCsv:function() {
		var data = [];
		for (var i = 0; i <= self.spectrum.length - 1; i++) {
			data.push([self.xfreq[i],self.spectrum[i]]);
		}
		var csvContent = "data:text/csv;charset=utf-8,";
		data.forEach(function(infoArray, index){
			dataString = infoArray.join(",");
			csvContent += index < data.length ? dataString+ "\n" : dataString;
		});
		var encodedUri = encodeURI(csvContent);
		window.open(encodedUri);
	}
};

function multiplier(array,factor){   // recordar que la mult en JS es IMPRECISA!
	var arraytemp = [];
	for (var i = 0; i <= array.length-1; i++) {
		arraytemp.push(array[i]*factor);
	}
	return arraytemp;
}
function adder(array1,array2){  // suma arrays
	try{
		if(array1.length != array2.length) throw "not same length";
	}
	catch(err){
		console.log("(adder) Error: "+ err+ ".");
		return null;
	}
	var arraytemp = [];
	for (var i =0; i<= array1.length - 1; i++) {
		arraytemp.push(array1[i]+array2[i]);
	}
	return arraytemp;
}


var data_srt1 = new dataObj(); // data for srt1
var data_srt2 = new dataObj(); // data for srt2

console.log('model.js executed successfully');


