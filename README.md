# Documentación (BORRADOR!) # 

ARIGUI is an user-interface designed to use the MIT Haystack radiotelescopes altogether with the SRT receivers, SignalHound receiver and ROACH interferometer. This software is by now intended to work with the ARI_API (Python), which uses the ARIAPI platform made in the Zero Ice framework.

The main functions are:
- Use of one antenna in Single Dish Mode
- Use of two antennae in Double Dish, Intensity Mode.
- Use of two antennae in Double Dish, Interferometer Mode (in development).
- Upload a simple script to make pre-programmed observations.

You will be capable of moving the antennae and obtaining spectral data and status of the telescope in almost real time.

Reseña
ARIGUI is a web application that uses Flask, Python, gunicorn and Flask-socketio, all of them mounted in an NGINX webserver. All this stuff is seen at client side as a fancy web inteface and capable of . This software is located on SRTcontrol computer (the black one at Santa Martina), at /home/srt/ari_gui. The use of Python makes possible the easy use of the ARIAPI (in Python too) that allows the (COMMANDING) the antennae via command-line.

You can access the application at http://www.astro.puc.cl:2080 <VERIFICAR

This application requires log-in. If you need the log-in information, ask Rolando Dünner or Juan Fluxá at the Instituto de Astronomia UC.

### How does it works? ###
When entering to the webpage, the application tries to connect to the ARIAPI server running on SRTcontrol. The antennae might be previously initialized, but the application itself verifies that everything is running fine. By this, there would not be any problems by using the antennae after any sudden disconnect.

After you login, the application will not do anything until you select an operation mode. These are:
- Single Dish, SRT1 Mode
- Single Dish, SRT2 Mode
- Double Dish, Intensity Mode

You should take a look at the SRT Manual or ask the professor if you do not understand what is each mode for.

Once you select the mode, the telescope will start initializing the services needed to use the antennae correctly and obtain the data. To check the list of ARIAPI-commands executed when changing to a particular mode, take a look at SDFKSDGNSLDGNSLDG.

After initialization, the spectrum plots will be shown and refreshed automatically at the center of the page. At the right side, you will see the general status information, which will be, for instance:
- Position in azimuthal and equatorial coordinates
- Spectrum information/configuration
- System temperature and calibration info
- Current time