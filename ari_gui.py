from gevent import monkey
monkey.patch_all()

import sys, os
sys.path.pop() # TEMPORAL FIX TO PATH PROBLEM
sys.path.append("/home/srt/ARI_LAST/ari/TCS/rpiSRT/src/clients/SRT_client")
sys.path.append("/home/srt/ARI_LAST/ari/TCS/rpiSRT/src/Icefiles")
sys.path.append("/home/srt/ARI_LAST/ari/TCS/rpiSRT/src/API")
sys.path.append("/home/srt/ARI_LAST/ari/TCS/rpiSRT/src/observingModes/")
print sys.path
print sys.executable
print os.path.realpath(__file__)
import logging
import json
import time, datetime
import ephem
from numpy import radians, degrees
import numpy as np
from ARI_API import *
from threading import Thread, Event
from flask import Flask, render_template, session, request, jsonify, \
current_app, copy_current_request_context, redirect
from flask.ext.socketio import SocketIO, emit, join_room, leave_room, \
close_room, disconnect
from werkzeug import secure_filename

UPLOAD_FOLDER = '/home/srt/submittedAriScripts'
ALLOWED_EXTENSIONS = set(['txt','ari'])
ALLOWED_COMMANDS_SCRIPT = set(['changeObsMode', 'makeObs','changeRxMode', 'offset','saveSpectra', \
	])

app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'secret!'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
socketio = SocketIO(app)

####### Setting up the logging system #######
logging.basicConfig(level=logging.DEBUG,format='[%(asctime)s][%(levelname)s] - %(funcName)s: %(message)s')
logger = logging.getLogger(__name__)

handler = logging.FileHandler('/home/srt/ari_gui/log/ari_gui.log')
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('[%(asctime)s][%(levelname)s] - %(funcName)s: %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)
logger.info('Starting the execution of ari_gui.py')

#f = open(os.devnull,'w')
#currentSpectraDate = ''
#spectraDateFirstTimeFlag = True
#systemReadyToObs = False

observer = ephem.Observer()
observer.lon = -1* radians(70.53)
observer.lat = -1* radians(33.2692)
observer.elevation = 1450
J0 = ephem.julian_date(0)

spectrumReadDelay = 2.5 #in seconds
statusReadDelay = 2
findRadecSourcesReadDelay = 100
stopArrayDelay = 2
enableSpectrumArrayDelay = 5

# declaring the object a, which is the API
a = None

# def spectrumRead(event):
# 	#logger.debug('Executing spectrumRead')
# 	time.sleep(0.2)
# 	while event.isSet():
# 		logger.debug('Will execute getLastSpectrumArray')
# 		a.getLastSpectrumArray()
# 		logger.debug('Already executed getLastSpectrumArray, now sleeping')
# 		time.sleep(spectrumReadDelay-0.2)
# 		logger.debug('Not sleeping anymore')
# 		if a.spectrumEnabled: #a.observing(bool):observation with array in progress
# 			logger.debug('a.spectrumEnabled is True!, now will execute isThereNewSpectra')
# 			isThereNewSpectra()

def spectrumRead(event):
	while True:
		event.wait()
		a.getLastSpectrumArray()
		logger.debug('Already executed getLastSpectrumArray, now sleeping')
		time.sleep(spectrumReadDelay-0.2)
		if a.spectrumEnabled: #a.observing(bool):observation with array in progress
			isThereNewSpectra()
			time.sleep(spectrumReadDelay-0.2)

def statusRead(event):
	while True:
		event.wait()
		#logger.debug('statusRead: getArrayStatus')
		a.getArrayStatus()
		#logger.debug('statusRead: getObsModeStatus')
		a.getObsModeStatus()
		time.sleep(statusReadDelay)
		emitApiStatus()
		emitArrayStatus()
		emitObsModeStatus()
		logger.info('emitted all status to webapp')

def findRadecSourcesRead(event):
	while True:
		event.wait()
		#sys.stdout = f
		a.findRadecSources()
		#sys.stdout = sys.__stdout__
		#emitApiStatus()
		time.sleep(findRadecSourcesReadDelay)


spectrumReadThreadReady = Event()
statusReadThreadReady = Event()
findRadecSourcesThreadReady = Event()

#spectrumReadThreadReady.clear()
#statusReadThreadReady.clear()
#findRadecSourcesThreadReady.clear()

spectrumReadThread = Thread(target=spectrumRead, args=(spectrumReadThreadReady,))
statusReadThread = Thread(target=statusRead, args=(statusReadThreadReady,))
findRadecSourcesThread = Thread(target=findRadecSourcesRead, args=(findRadecSourcesThreadReady,))

spectrumReadThread.start()
statusReadThread.start()
findRadecSourcesThread.start()

#@socketio.on('sendApiStatusEvent', namespace='/telescope')
def emitApiStatus():
	rd = []
	for sour in a.radecSources:
		rd.append(dict(source=sour.source, az=sour.az, el=sour.el))
	with app.test_request_context('/telescope'):
		#logger.info('emiting now api status to webapp')
		socketio.emit('receiveApiStatusEvent',json.dumps(dict(connected=a.connected, obsMode=a.obsMode, \
		arraySetup=a.arraySetup, RxArray=a.RxArray, observing=a.observing, \
		observation=a.observation, spectrumEnabled=a.spectrumEnabled, \
		scanMapinProgress=a.scanMapinProgress, offsets=a.offsets, \
		radecSources= rd)),namespace='/telescope')

#@socketio.on('sendArrayStatusEvent', namespace='/telescope')
def emitArrayStatus():
	arr = {}
	for antenna in a.arrayStatus.keys():
		t = a.arrayStatus[antenna]
		[ra,dec] = azel2radec([t.az,t.el])
		[ranow,decnow] = azel2radec([t.aznow,t.elnow])
		arr[antenna]=dict(name=t.name, time=t.time, SRTState=t.SRTState, SRTonTarget=t.SRTonTarget,\
			SRTMode=t.SRTMode, SRTTarget=t.SRTTarget, SRTTrack=t.SRTTrack, enObs=t.enObs, \
			newAzEl=t.newAzEl, enSRT=t.enSRT, enSpec=t.enSpec, slewing=t.slewing, \
			cmdstop=t.cmdstop, IsMoving=t.IsMoving, getStatus=t.getStatus, \
			portInUse=dict(InUse=t.portInUse.InUse, Routine=t.portInUse.Routine), \
			spectra=t.spectra, RxSwitchMode=t.RxSwitchMode, toSource=t.toSource, \
			SRTinitialized=t.SRTinitialized, initialized=t.initialized, Target=t.Target, obsTarget=t.obsTarget, \
			az=t.az, el=t.el, aznow=t.aznow, elnow=t.elnow, azoffset=t.azoffset, \
			eloffset=t.eloffset, axis=t.axis, tostow=t.tostow, elatstow=t.elatstow,\
			azatstow=t.azatstow, slew=t.slew, serialport=t.serialport, \
			lastSRTCom=t.lastSRTCom, lastSerialMsg=t.lastSerialMsg, ra=ra, dec=dec, ranow=ranow, decnow=decnow)
	with app.test_request_context('/telescope'):
		#logger.info('emiting now array status to webapp')
		socketio.emit('receiveArrayStatusEvent', json.dumps(arr), namespace='/telescope')

def emitObsModeStatus():
	arr = {}
	t = a.obsModeState # in the next line I deleted SRTcalibrated!! # NOTE THAT SRTRxSetup is not HERE!
	arr = dict(ARIcontrollers = t.ARIcontrollers, ArrayFrequency = t.ArrayFrequency, \
		ArrayMovingToTarget = t.ArrayMovingToTarget, ArrayOffsets = t.ArrayOffsets, \
		ArrayOnTarget = t.ArrayOnTarget, ArrayRxMode = t.ArrayRxMode, \
		ArrayStopCommand = t.ArrayStopCommand, NewSpectrumToRead = t.NewSpectrumToRead,\
		ReadSpectrum = t.ReadSpectrum, RxSwitchMode = t.RxSwitchMode, \
		ScanMapInProgress = t.ScanMapInProgress, \
		WaitingSpectrum = t.WaitingSpectrum, atStow = t.atStow, getClientStatus = t.getClientStatus,\
		initialized = t.initialized, mode = t.mode, nodes = t.nodes, observingMode = t.observingMode,\
		setupInProgress = t.setupInProgress, stowInProgress = t.stowInProgress)
	with app.test_request_context('/telescope'):
		#logger.info('emiting now ObsModeStatus to webapp')
		socketio.emit('receiveObsModeStatusEvent', json.dumps(arr), namespace='/telescope')

# def isThereNewSpectra():
# 	#a.getLastSpectrumArray()
# 	if a.spectrum:
# 		print('NEW spectra found!')
# 		if a.obsMode[1]=='Double':
# 			ss1 = a.spectrum['SRT1'].sampleStamp
# 			ss2 = a.spectrum['SRT2'].sampleStamp
# 			sampleStamp1 = dict(name=ss1.name, timdate=ss1.timdate, aznow=ss1.aznow, elnow=ss1.elnow,\
# 				temperature=ss1.temperature, freq0=ss1.freq0, av=ss1.av, avc=ss1.avc)
# 			sampleStamp2 = dict(name=ss2.name, timdate=ss2.timdate, aznow=ss2.aznow, elnow=ss2.elnow,\
# 				temperature=ss2.temperature, freq0=ss2.freq0, av=ss2.av, avc=ss2.avc)
# 			jsondata = json.dumps(dict(SRT1=dict(spec=a.spectrum['SRT1'].specd,sampleStamp=sampleStamp1), \
# 				SRT2=dict(spec=a.spectrum['SRT2'].specd,sampleStamp=sampleStamp2)))
# 			print(jsondata)
# 		elif a.obsMode[1]=='SRT1' or a.obsMode[1]=='SRT2':
# 			print(a.obsMode[1])
# 			tel = a.obsMode[1]
# 			ss1 = a.spectrum[tel].sampleStamp
# 			sd = a.spectrum['SRT1']
# 			sdnumpy = np.asarray(a.spectrum['SRT1'].specd)
# 			# print('sdnumpy')
# 			# print(sdnumpy)
# 			# print('sd = a.spectrum[SRT1], sd.keys')
# 			# print(sd.keys)
# 			# print('sd.specd')
# 			# print(sd.specd)
# 			# print('a.spectrum[tel].specd')
# 			# print(a.spectrum[tel].specd)
# 			# specd = a.spectrum[tel].specd
# 			# print('specd')
# 			# print(specd)
# 			sampleStamp = dict(name=ss1.name, timdate=ss1.timdate, aznow=ss1.aznow, elnow=ss1.elnow,\
# 				temperature=ss1.temperature, freq0=ss1.freq0, av=ss1.av, avc=ss1.avc, \
# 				nfreq=ss1.nfreq, freqsep=ss1.freqsep)
# 			jsondata = json.dumps(dict(SRT=dict(specd = specd, sampleStamp=sampleStamp)))
# 			print(jsondata)
# 		return True
# 	else:
# 		return False

def isThereNewSpectra():
	#a.getLastSpectrumArray()
	if a.spectrum:
		if a.obsMode[1]=='SRT1' or a.obsMode[1]=='SRT2':
			print(a.obsMode[1])
			tel = a.obsMode[1]
			ss1 = a.spectrum[tel].sampleStamp
			sd = a.spectrum['SRT1']
			sdnumpy = np.asarray(a.spectrum['SRT1'].specd)
			print("---------------sdnumpy=np.asarray(a.spectrum['SRT'].specd")
			print(sdnumpy)
			#print('sd = a.spectrum[SRT1], sd.keys')
			#print(sd.keys)
			print('---------------sd.specd')
			print(sd.specd)
			print('------------------a.spectrum[tel].specd')
			print(a.spectrum[tel].specd)
			specd = a.spectrum[tel].specd
			print('------------------------specd')
			print(specd)
			sampleStamp = dict(name=ss1.name, timdate=ss1.timdate, aznow=ss1.aznow, elnow=ss1.elnow,\
				temperature=ss1.temperature, freq0=ss1.freq0, av=ss1.av, avc=ss1.avc, \
				nfreq=ss1.nfreq, freqsep=ss1.freqsep)
			jsondata = json.dumps(dict(SRT=dict(specd = specd, sampleStamp=sampleStamp)))
			print("----------------- PRINT JSONDATA-----------------")
			print(jsondata)
			with app.test_request_context('/telescope'):
				logger.info('emiting now spectrum to webapp')
				socketio.emit('receiveSpectraEvent', jsondata, namespace='/telescope')
		return True
	else:
		return False

def objToDict(obj):
	temp = {}
	attributes = [c for c in dir(obj) if not c.startswith('__')]
	for att in attributes:
		temp[att] = eval('obj.'+att)
	return temp

def waitUntil(cond, excm='',timeres=0.1, timeout=15): # this receives the name of the variable, not so elegant
	logger.debug('variable: %s',cond)
	start = time.time()
	while not eval(cond):
		if (time.time() - start) > timeout:
			break
		time.sleep(timeres)
	if not eval(cond):
		raise Exception(excm)

@app.route('/')
def index():
	return render_template('base.html')

@app.route('/link')
def testlink():
	return render_template('telescope.html', mode='SRT1')

# remember to check if the user is logged in
@app.route('/SD/<teles>')
def SD(teles):
	ariConnect()
	logger.info(teles)
	changeObsMode(dict(mode='SD',inst=teles))
	return render_template('telescope.html', mode=teles)

@app.route('/double')
def double():
	changeObsMode(dict(mode='SD',inst='Double'))
	return render_template('telescope.html', mode='double')

@socketio.on('connect', namespace='/telescope')
def connect_init():
	socketio.emit('testResponse', {'data': 'IM ALIVE!'}, namespace='/telescope')
	pass

def ariConnect():
	logger.debug('Executing ariConnect')
	time.sleep(3)
	global a
	if a == None:
		spectrumReadThreadReady.clear()
		statusReadThreadReady.clear()
		findRadecSourcesThreadReady.clear()
		logger.debug('a == None! Instantiating API()')
		a = API()
	try: # Try to connect
		a.connect('localhost -p 10015')
		#waitUntil('a.connected','Could not connect to host')
	except Exception as e:
		logger.critical(repr(e)+ '. At this point you should start the ari_gui daemon with sudo start ari_gui.')
		socketio.emit('testResponse', {'data': repr(e)})
		raise
	else:
		logger.debug('From ari_gui.py now it is visible that API is connected!')
		socketio.emit('testResponse', {'data': 'Connected!'}, namespace='/telescope')

# @socketio.on('disconnect', namespace='/telescope')
# def ariDisconnect():
# 	logger.debug('Disconnecting client from server')
# 	spectrumReadThreadReady.clear()
# 	statusReadThreadReady.clear()
# 	findRadecSourcesThreadReady.clear()
# 	if a.arrayStatus and a.arrayStatus[a.arrayStatus.keys()[0]].enSpec:
# 		a.disableSpectrumArray()
# 	a.stopArray()
# 	#a.stowArray()
# 	a.disconnect()
# 	logger.info('Client disconnected')

@socketio.on('changeObsModeEvent', namespace='/telescope')
def changeObsMode(m): # mode is an array with 2 strings
	#spectrumReadThreadReady
	mode = [m['mode'],m['inst']]
	logger.debug('Stopping spectrum and array read threads...')
	spectrumReadThreadReady.clear()
	statusReadThreadReady.clear()
	findRadecSourcesThreadReady.clear()
	logger.debug('Done!')
	time.sleep(max(spectrumReadDelay,statusReadDelay)+0.5)

	# we have to verify if we can read the array status. If not, the telescope probably
	# has not been initialized.
	#try:
	#	a.getArrayStatus()
	#except Exception as e:
	#	logger.info('getArrayStatus: could not be completed. Maybe it is the first time initializing.')
	#	logger.error(repr(e))

	# making sure we can securely change the observing mode. Need to check if this is necessary
	
	if a.arrayStatus:
		if a.arrayStatus[a.arrayStatus.keys()[0]].enSpec:
			logger.debug('spectra reading is enabled. Disabling before changing obsmode')
			a.disableSpectrumArray()
			waitUntil('not a.arrayStatus[a.arrayStatus.keys()[0]].enSpec','Could not disable the spectra reading',1,20)
		if a.arrayStatus[a.arrayStatus.keys()[0]].slew: #very unlikely, but one never knows
			logger.error('antenna is slewing!!')
			return False
		if a.arrayStatus[a.arrayStatus.keys()[0]].enObs:
			logger.debug('Observation array is enabled. Disabling before changing obsmode')
			a.stopArray()
			waitUntil('a.arrayStatus[a.arrayStatus.keys()[0]].enObs','Could not stop array',0.5,20)

	try: # if the telescope was initialized previously this will work anyway
		a.setObservingMode(mode[0],mode[1])
		time.sleep(1)
		a.createObservingMode()
		logger.debug('Creating observing mode with options %s, %s', mode[0], mode[1])
		waitUntil('a.obsModeCreated','Could not create the observing mode')
		logger.debug('Observing mode %s %s created succesfully', mode[0],mode[1])
		
		statusReadThreadReady.set() # start reading the array status again
		findRadecSourcesThreadReady.set()

		a.setupArray()
		#logger.debug('Setting up array')
		waitUntil('a.arraySetup', 'Could not set up array')
		logger.debug('Array set up succesfully')
		
		logger.info('Waiting for array to go to STOW (timeout=800s)')
		for telescope in a.arrayStatus.keys():
			waitUntil(('a.arrayStatus[\''+telescope+'\'].tostow'),telescope+' will not go to STOW',0.5,800)
			logger.info(telescope+' is in STOW.')
		a.setRxArray(1420.4, 1) # just the default option
		logger.info('Array initialized succesfully.')
	except Exception as e:
		logger.critical(repr(e))
		#raise
	else:
		logger.info('Mode changed to %s %s', mode[0],mode[1])
		socketio.emit('changeObsModeReadyEvent')

@socketio.on('changeRxModeEvent', namespace='/telescope')
def changeRxMode(m):
	freq = m['freq']
	rxmode = m['rxmode']
	a.setRxArray(freq,rxmode)

@socketio.on('offsetEvent', namespace='/telescope')
def offset(m):
	azoff = m['azoff']
	eloff = m['eloff']
	a.setOffsetPointing(azoff,eloff)

@socketio.on('makeObsEvent', namespace='/telescope')
def makeObs(m):
	a.findRadecSources()
	spectrumReadThreadReady.clear()
	time.sleep(2)
	mode = m['mode']
	target = m['target']
	a.stopArray()
	time.sleep(stopArrayDelay)
	logger.info('Starting observation with mode %s. Going to %s',mode,str(target))
	if mode == 'GoTo':
		target = map(float,target)
	a.observeWithArray(mode,target)
	#logger.info('sleeping 15 seconds ----------')
	time.sleep(5)
	for telescope in a.arrayStatus.keys():
		logger.debug('Waiting for %s to finish slewing', telescope)
		waitUntil(('not a.arrayStatus[\''+telescope+'\'].slewing'),telescope+' is taking too long to slew',0.5,800)
	a.enableSpectrumArray()
	time.sleep(enableSpectrumArrayDelay)
	#a.getLastSpectrumArray()
	logger.info('Starting reading spectrum')
	spectrumReadThreadReady.set()


def allowed_file(filename): # from documentation of pocoo guys
	return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def azel2radec(pos):
	az = radians(pos[0])
	el = radians(pos[1])
	observer.date = ephem.julian_date() - J0
	ra,dec = observer.radec_of(az,el)
	return [degrees(ra),degrees(dec)]

@app.route('/script', methods=['GET', 'POST']) # from documentation of pocoo guys
def upload_file():
	if request.method == 'POST':
		file = request.files['file']
		if file and allowed_file(file.filename):
			#filename = secure_filename(file.filename)
			now = time.strftime("%Y%m%d-%H%M%S")
			filename = 'script.txt'
			os.mkdir(os.path.join(app.config['UPLOAD_FOLDER'], now))
			file.save(os.path.join(app.config['UPLOAD_FOLDER'], now, filename))
			return redirect(url_for('uploaded_file',folder=os.path.join(app.config['UPLOAD_FOLDER'], now)))
	return render_template('script.html')

@app.route('/uploaded_file')
def uploaded_file(folder):
	scriptLog = logging.FileHandler(os.path.join(folder,'obs.log'))
	scriptLog.setLevel(logging.INFO) # DESPUes IMPLEMENTAR OPCIon!!!
	scriptLogFormatter = logging.Formatter('[%(asctime)s][%(levelname)s] - %(funcName)s: %(message)s')
	scriptLog.setFormatter(scriptLogFormatter)
	logger.addHandler(handler)
	script = file(os.path.join(folder,'script.txt'),'r')
	#verificar aqui la sintaxis del script
	for line in script.readlines():
		eval(line)
	#implementar mecanismo para mostrar cosas


if __name__ == '__main__':
	socketio.run(app, port=8000, log_output=True)
